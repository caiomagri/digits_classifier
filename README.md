## Como rodar
Essa é uma aplicação simples que utiliza um dataset já existente na biblioteca do sklearn, onde tenta classificar uma
imagem que representa uma respectivo digito.

Criando um container e rodando a aplicação.

```bash
docker compose up -d
```

## Como acessar a aplicação?

A aplicação por default irá rodar na porta 8080 do seu localhost.
Para realizar testes, voce pode utilizar o arquivo tests.http direto 
no seu editor caso o mesmo tenha suporte e framework para rodar *.http ou 
realizar uma chamada http do tipo POST

```bash
POST http://localhost:8080/v2/models/digits_classifier/versions/v0.1.0/infer HTTP/1.1
Content-Type: application/json

{
    "id": "request-0",
    "inputs": [
        {
            "name": "predict",
            "shape": [
                1, 64
            ],
            "datatype": "INT32",
            "data": [0.0,0.0,1.0,11.0,14.0,15.0,3.0,0.0,0.0,1.0,13.0,16.0,12.0,16.0,8.0,0.0,0.0,8.0,16.0,4.0,6.0,16.0,5.0,0.0,0.0,5.0,15.0,11.0,13.0,14.0,0.0,0.0,0.0,0.0,2.0,12.0,16.0,13.0,0.0,0.0,0.0,0.0,0.0,13.0,16.0,16.0,6.0,0.0,0.0,0.0,0.0,16.0,16.0,16.0,7.0,0.0,0.0,0.0,0.0,11.0,13.0,12.0,1.0,0.0
            ]
        }
    ]
}
```